function [E, D] = proc_entropy(s, NumBins)
% [E, D] = proc_entropy(s)
%
% Computes the entropy of the input signal for each channel separetely. It
% returns the entropy values of each channel and the distribution.
% Input signal must be in the format samples x channels.
% THe distribution of the sample is supposed to be Gaussian.

    if ismatrix(s) == false
        error('chk:input', 'Input signal must be in the format points x channels');
    end

    NumSamples = size(s, 1);
    NumChannels = size(s, 2);

    if nargin < 2
        NumBins = log2(NumSamples) + 1;
    end
        
    D = zeros(floor(NumBins), NumChannels);
    E = zeros(NumChannels, 1);
    
    for chId = 1:NumChannels
        
        cs = s(:, chId);
        
        cdistr  = hist(cs,NumBins)./ sum(~isnan(cs));
        cent    = -sum(cdistr(cdistr~=0).*log2(cdistr(cdistr~=0)));
    
        D(:, chId) = cdistr;
        E(chId)    = cent;
    end
end