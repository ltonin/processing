function [bandpower, envelop, filtered, info] = proc_bandpower(s, band, samplerate, varargin)


    % Default parameters
    dorder      = round(3*samplerate/band(1));
    dtransition = 0.2;
    ddolog      = true;
    dshow       = false;
    
    % Validation function
    isdata = @(x) assert(ismatrix(x) & isnumeric(x), 'Wrong format in the data argument');
    isband = @(x) assert(isnumeric(x) & isvector(x) & length(x) == 2 & diff(x) > 0, 'Wrong format in the band argument');

    % Add parameters
    p = inputParser;
    p.addRequired('s', isdata);
    p.addRequired('band', isband);
    p.addRequired('samplerate', @isnumeric);
    p.addParameter('transition', dtransition, @isscalar);
    p.addParameter('order', dorder, @isnumeric);
    p.addParameter('dolog', ddolog, @islogical);
    p.addParameter('show', dshow, @islogical);
    
    parse(p, s, band, samplerate, varargin{:});
    transition = p.Results.transition;
    order      = p.Results.order;
    dolog      = p.Results.dolog;
    show       = p.Results.show;
    
    
    nyquist = samplerate./2;
    ffreqs = [0 (1-transition)*band(1) band(1) band(2) (1+transition)*band(2) nyquist]/nyquist;
    ideal  = [0 0 1 1 0 0];
    
    weights = firls(order, ffreqs, ideal);
    
    filtered = zeros(size(s));
    hilberttf = zeros(size(s));
    for chId = 1:size(s, 2)
        filtered(:, chId) = filtfilt(weights, 1, double(s(:, chId)));
        hilberttf(:, chId) = hilbert(filtered(:, chId));
    end

    envelop   = abs(hilberttf);
    bandpower = envelop.^2;
    
    if dolog == true
        bandpower = 10*log10(bandpower);
    end
    
    info.order = order;
    info.transition = transition;
    
    if show == true
        figure;
        subplot(2, 1, 1);
        plot(ffreqs*nyquist, ideal, 'k--o', 'markerface', 'r');
        ylim([-.1 1.1]);
        xlim([-2 nyquist+2]);
        xlabel('frequencies [Hz]');
        ylabel('amplitude');
        grid on;
        
        subplot(2, 1, 2);
        plot((0:order)*(1000/samplerate), weights);
        xlabel('time [ms]');
        ylabel('amplitude');
        grid on;
    end

end
