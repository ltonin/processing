function m = proc_movingaverage(s, wsize, wshift)
% m = proc_movingaverage(s, win)
%
% Given a signal s [samples x channels], it computes the moving average
% on each channel with a window of size win. It uses filtfilt to avoid
% delay.
    
    if nargin == 2
        wshift = 1;
    end

    NumSamples  = size(s, 1);
    NumChannels = size(s, 2);
    
    
    NumSteps = 1 + floor((NumSamples - wsize)/wshift);
    NumSteps = NumSamples/wshift - 1;
    mavg_B    = (1/NumSteps)*ones(1,NumSteps);
    mavg_step = wshift;
    
    mavg_A  = 1;
%     mavg_B  = zeros(1,NumSamples);
%     mavg_B(1:wshift:end) = 1;
%     mavg_B  = mavg_B/sum(mavg_B);

    m = zeros(NumSamples, NumChannels);
   
    for chId = 1:NumChannels
        m(:, chId) = filter(mavg_B, mavg_A, s(:, chId));
    end
    
    m = m(1:wshift:end, :);
end