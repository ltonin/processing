function F = proc_fisher(P, Pk, NSTD)
% F = proc_fisher(P, Pk, NSTD)
% 
% This function computes the fisher score for each pair channel-frequency,
% across trials.
%   - P 	data matrix [samples x frequencies x channels x trials]. 
%   - Pk    label vector [trials x 1]
%
% The function takes all the samples to build the distributions of the 
% two classes and compute the fisher score.
%
% It returns a vector F of fisher score. The vector is in the format
% [(channelsxfrequencies) x 1]
    
    if nargin == 2
        NSTD = [];
    end
    
    Classes = unique(Pk);
    NumClasses = length(Classes);
    
    if NumClasses ~= 2
        error('chk:classes', 'Number of classes must be 2');
    end
    
    P1 = P(:, :, :, Pk == Classes(1));
    P2 = P(:, :, :, Pk == Classes(2));

    D1 = proc_reshape_ts_bc(P1);
    D2 = proc_reshape_ts_bc(P2);
    
    
    F = zeros(size(D1, 2), 1);
    for vId = 1:size(D1, 2)
        
        % Getting current data for each class and the given feature
        cdata1 = D1(:, vId);
        cdata2 = D2(:, vId);
        
        % If rmsize is provided, remove outlayers per class
        if isempty(NSTD) == false
            out1 = proc_get_outlayers(cdata1, NSTD);
            out2 = proc_get_outlayers(cdata2, NSTD);
            cdata1 = cdata1(out1 == false);
            cdata2 = cdata2(out2 == false);
        end
        
        % Computing mean and standard deviation for each class
        m1 = nanmean(cdata1);
        s1 = nanstd(cdata1);
        
        m2 = nanmean(cdata2);
        s2 = nanstd(cdata2);
        
        % Computing feature score for the given feature
        F(vId) = abs(m2 - m1) ./ sqrt(s1.^2 + s2.^2);
        
    end
    
  
%     m1 = mean(D1, 1);
%     m2 = mean(D2, 1);
% 
%     s1 = std(D1, [], 1);
%     s2 = std(D2, [], 1);
% 
%     F = abs(m2 - m1) ./ sqrt(s1.^2 + s2.^2);
    

end