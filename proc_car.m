function [out, car] = proc_car(in, varargin)
% [out, car] = proc_car(datain, excluded)
%
% Removes car values from the data.
% datain is [points x channels] matrix

    defaultIncluded = [];
    defaultExcluded = [];


    % Error messages
    error_msg_2d_matrix = 'Value must be a 2D matrix';
    error_msg_list_channels = 'Value must be a numeric row/column with channel index';

    % Validation functions
    is_2d_matrix = @(x) assert(ismatrix(x), error_msg_2d_matrix);
    is_list_channels = @(x) assert( ((isrow(x) || iscolumn(x)) && isnumeric(x)) || isempty(x), error_msg_list_channels);

    % Input parser
    p = inputParser;
    p.addRequired('in', is_2d_matrix);
    p.addParameter('included', defaultIncluded, is_list_channels);
    p.addParameter('excluded', defaultExcluded, is_list_channels);

    % Parse input
    parse(p, in, varargin{:});

    if(isempty(p.UsingDefaults))
        error('''included'' and ''excluded'' parameters cannot be used simuultaneously')
    end

    
    if isempty(p.Results.included) == false
        chidx = get_index_inclusion(1:size(in, 2), p.Results.included);
    elseif isempty(p.Results.excluded) == false
        chidx = get_index_exclusion(1:size(in, 2), p.Results.excluded);
    else
        chidx = 1:size(in, 2);
    end

    
    car = mean(in(:, chidx), 2);
    out = in - car * ones(1, size(in, 2));
    
end


function index = get_index_exclusion(chanidx, excluded)
    index = setdiff(chanidx, excluded);
end

function index = get_index_inclusion(chanidx, included)
    
    [value, index] = intersect(chanidx, included);

    if length(value) ~= length(included)
        nochans = setdiff(included, value);

        for i = 1:length(nochans)
            warning(['Channel with id ' num2str(nochans(i)) ' requested but not available']);
        end
    end
end

