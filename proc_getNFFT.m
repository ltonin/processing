function [nfft, nfreq] = proc_getNFFT(winlength)
% [nfft, nfreq] = proc_getNFFT(winlength)
%
% Given a window length, the function returns the NFFT and the number of 
% frequencies resulting from a pwelch or a periodogram.
%
% SEE also: pwelch, periodogram

    nfft   = max(2^nextpow2(winlength), 256);

    if mod(nfft, 2) == 0        % Even
        nfreq = (nfft/2 + 1);
    else                        % Odd
        nfreq = (nfft+1)/2;
    end
    
end