function [psd, frequencies] = proc_pwelch(s, window, overlap, selfreqs, fs, dolog)
% [psd, frequencies] = proc_pwelch(s, window, overlap, selfreqs, fs, [dolog])
%
% Computes the pwelch of the input signal for each channel. Window and overlap are 
% expressed in samples. Optional output is the vector of frequencies.

    if nargin < 6
        dolog = true;
    end

    NumFreqs    = length(selfreqs);
    NumChannels = size(s, 2);
    psd = zeros(NumFreqs, NumChannels);
    
    for chId = 1:NumChannels
       
        [pxx, frqs] = pwelch(s(:, chId), window, overlap , [], fs);
       
        [~, set] = intersect(frqs, selfreqs);
        
        if(length(set) ~= length(selfreqs))
            warning('chk:frq', '[proc_pwelch] Cannot provide requested frequencies');
        end
        
        cpsd = pxx(set);
        
        if dolog
            cpsd = log(cpsd);
        end
        
        psd(:, chId) = cpsd;
        
    end

    if nargout > 1
        frequencies = frqs(set);
    end



end