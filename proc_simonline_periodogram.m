function [psd, settings] = proc_simonline_periodogram(s, fs, fsize, bsize, wtype, freqrange)
% [psd, settings] = proc_simonline_periodogram(s, fs, fsize, bsize [, wtype, freqrange])
%
% This function simulates the online processing loop with following step
% (in this order):
%   1 - Ring buffer (length: bsize) filled at each iteration with a frame 
%       of the data s (length: fsize)
%   2 - Periodogram computation with selected window (length: bsize) 
% 
% Spatial filtering (e.g., CAR, Laplacian, CSP) is not applied since 
% usually does not depend on temporal dimension and can be safetely applied
% before.
%
% Input arguments:
%
%   s               Data to be processed [samples x channels]
%   fs              Sample rate of the data
%   fsize           Length of frame size of the data to be added to
%                   ringbuffer [in sample]
%   bsize           Length of the ringbuffer [in sample]
%   wtype           Type of the window to be applied to s. By the default
%                   an Hamming windows is used. Possible windows:
%                   - 'hamming'
%                   - 'hann'
%                   - 'blackman'
%                   - 'flattop'
%   freqrange       Requested frequencies. If not provided standard number
%                   of FFT is used (the maximum of 256 or the next power 
%                   of two greater than the window length)
%
% Output arguments:
%
%   psd             Three dimensional matrix with psd value computed on the 
%                   ringbufferat each frame 
%                   [samples x frequencies x channels]
%   settings        Settings structure of psd data with following
%                   information:
%                   .fs         Sample Rate [Hz]
%                   .bsize      Length of the ringbuffer [samples]
%                   .fsize      Length of the frame [samples]
%                   .wtype      Type of the window applied to the signal
%                   .wsize      Length of the window [samples]
%                   .nfft       Number of FFT points where PSD has been
%                               computed.
%                   .f          Vector with frequency values where PSD has 
%                               been computed [frequency x 1]
%
% Example:
%
% Simulated online processing for 10 seconds of sinwave (fc = 10Hz) with 
% samplerate of 512 Hz. Ringbuffer size is set to 1 second, frame size at 
% 0.0625 seconds. Periodogram window is set to 0.5 seconds.
%
% fs      = 512;             % Hertz
% T       = 10;              % seconds
% t       = 0:1/fs:T-1/fs;
% fc      = 10;              % Hertz
% s       = repmat(sin(t*fc*2*pi), [16 1])';
% fsize   = 0.0625;   % seconds
% bsize   = 1;        % seconds
% wtype   = 'hamming';
%
% [psd, settings] = proc_simonline_periodogram(s, fs, fsize, bsize, wtype);
%
% SEE ALSO: proc_ringbuffer_init, proc_ringbuffer_add, periodogram,
% hamming, hann, blackman, flattop

    if nargin < 5
        wtype = 'hamming';
        freqrange = [];
    end
    
    if nargin < 6
        freqrange = [];
    end

    NumSamples  = size(s, 1);
    NumChannels = size(s, 2);

    FrameSize   = floor(fsize*fs);
    BufferSize  = floor(bsize*fs);
    WinSize     = floor(bsize*fs);
    Window      = periodogram_window(wtype, WinSize);
    
    if isempty(freqrange) == true
        [NFFT, NumFreqs] = proc_getNFFT(WinSize);
    else
        if length(freqrange) == 1
            NFFT = freqrange;
            NumFreqs = NFFT/2 + 1;
        else
            NFFT = freqrange;
            NumFreqs = length(NFFT);
        end
    end
    
    FrameStart = 1:FrameSize:(NumSamples - FrameSize);
    FrameStop  = FrameStart + FrameSize - 1;
    NumFrames  = length(FrameStart);
    
    psd     = zeros(NumFrames, NumFreqs, NumChannels);
    rbuffer = proc_ringbuffer_init(BufferSize, NumChannels);
    
    fprintf('[proc] - Periodogram simonline process settings:\n');
    fprintf('         - Sample rate:        %d\n', fs);
    fprintf('         - Frame size:         %d\n', FrameSize);
    fprintf('         - Buffer size:        %d\n', BufferSize);
    fprintf('         - PSD window type:    %s\n', wtype);
    fprintf('         - PSD window size:    %d\n', WinSize);
    fprintf('         - PSD NFFT:           %d\n', NFFT);
    fprintf('         - PSD Frequency:      %d\n', NumFreqs);
    
    fprintf('[proc] - Processing each frame:\n');
    for frId = 1:NumFrames
        util_disp_progress(frId, NumFrames, '        ');
        cstart = FrameStart(frId);
        cstop  = FrameStop(frId);
        
        cframe = s(cstart:cstop, :);
        
        [rbuffer, isfull] = proc_ringbuffer_add(rbuffer, cframe);
        
        if isfull == false
            continue;
        end

        for chId = 1:NumChannels 
            [psd(frId, :, chId), f] = periodogram(rbuffer(:, chId), Window, NFFT, fs);
        end
    end
    
    settings.type    = 'periodogram';
    settings.fs      = fs;
    settings.bsize   = BufferSize;
    settings.fsize   = FrameSize;
    settings.wtype   = wtype;
    settings.wsize   = WinSize;
    settings.nfft    = NFFT;
    settings.f       = f;   
end

function window = periodogram_window(wtype, wsize)
    if (strcmpi(wtype, 'hamming'))
        window = hamming(wsize);
    elseif (strcmpi(wtype, 'hann'))
        window = hann(wsize);
    elseif(strcmpi(wtype, 'blackman'))
        window = blackman(wsize);
    elseif(strcmpi(wtype, 'flattop'))
        window = flattopwin(wsize);
    else
        error('chk:win', '[proc_simonline_periodogram] - Unknown window type');
    end
end