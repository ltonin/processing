function [mirr_s, delimiter] = proc_mirror(s, debug)
% [mirr_s, delimiter] = proc_mirror(s, fig)
%
% This function mirrors the input signal, by rotating the original signal
% on the left and on the righ side. The rotated version are flipped as well
% by substracting the 2*s(1) (left side) and 2*s(end) (right side), to come
% up with a continous output.
%
% Signal is in the format [points x channel] and the debug flag ([default value
% false]) allows to plot the output signal. If channel dimension is
% greater than 1, the mirroring is applied on each channel separately. The
% output signal size is 3 times the input signal.
% 
% The function also returns the samples position of the original signals in
% the mirrorred one
%
% SEE ALSO: rot90

    if nargin < 2
        debug = false;
    end
    
    NumSamples  = size(s, 1);
    NumChannels = size(s, 2);
    
    mirr_s = zeros(3*NumSamples, NumChannels);
    
    for chId = 1:NumChannels
        
        s_c = s(:, chId);
        
%         s_l = 2*s_c(1) - rot90(s_c, 90);
%         s_r = 2*s_c(end) - rot90(s_c, 90);
        
        s_l = rot90(s_c, 90);
        s_r = rot90(s_c, 90);
        
        mirr_s(:, chId) = [s_l; s_c; s_r];
    end
    
    delimiter = [NumSamples+1 2*NumSamples];
    
    
     if debug
        NumRows = 3;
        
        for chId = 1:NumChannels
            subplot(NumRows, ceil(NumChannels/NumRows), chId);
            
            plot(mirr_s(:, chId));

            hold on;
            plot([nan(size(s, 1), 1); s(:, chId); nan(size(s, 1), 1)], 'r');
            hold off;
            title(['Mirrored signal. Channel ' num2str(chId)]);
            grid on;
        end
        
        
        
    end
end