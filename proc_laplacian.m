function data = proc_laplacian(s, mask)
% data = proc_laplacian(s, mask)
%
% The function applies laplacian derivation by means of matrix
% multiplication between the input signal and a proper laplacian mask.
%
% SEE ALSO: proc_laplacian_mask, proc_get_montage

    data = s*mask;

end