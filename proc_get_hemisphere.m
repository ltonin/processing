function hemisphere = proc_get_hemisphere(chlabel, hemlabel)
% hemisphere = proc_get_hemisphere(chlabel, [hemlabel])
%
% Based on electrode label, the function returns if it belongs to left,
% right hemisphere or middle line. The assumption is that channels 
% with even ids are placed in the right hemisphere and with odd ids are
% placed in the left hemisphere. Channels without ids at the end of the
% label are assumed belonging to the middle line.
%
% By default hemlabels are [1 2 3] for left, right and middle line.
%
% The function accepts cell array for labels.
%
% The function returns a vector with the hemisphere id.
    if nargin < 2
        hemlabel = [1 2 3];
    end
    
    if iscell(chlabel) == false
        chlabel = {chlabel};
    end
    
    NumLabels = length(chlabel);
    hemisphere = zeros(NumLabels, 1);
    
    channel = regexp(chlabel, '\w*(?<number>\d)', 'names');
    
    for lId = 1:NumLabels
        if isempty(channel{lId})
            hmId = 3;
        else
            hmId = 1;
            if mod(channel{lId}.number, 2) == 0
                hmId = 2;
            end
        end
        hemisphere(lId) = hemlabel(hmId);
    end
end