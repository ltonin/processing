function Rs = proc_rectification(s, order, highband, lowband, fs)

    NumChannels = size(s, 2);
    Rs = zeros(size(s));
    
    for chId = 1:NumChannels
        
        cs = s(:, chId);
        
        % High-pass filter (band-pass)       
        hs = filt_highlow(cs, order, highband, fs, 'high');
        
        % Rectification        
        as = abs(hs);
        
        % Low-pass
        ls = filt_highlow(as, order, lowband, fs, 'low');
        
        Rs(:, chId) = ls;
    end

end
