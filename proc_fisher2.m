function F = proc_fisher2(P, Pk, NSTD, do_balance)
% F = proc_fisher2(P, Pk [, NSTD])
% 
% The function computes the fisher score for each pair channel-frequency.
% Nan values are not considered. Optional argument NSTD to remove outlayers 
% (number of standard deviation to consider a sample as outlayer). 
% If required, the outlayer removal is performed for each class, separately. 
%
% Input:
%   - P         data matrix  [samples x frequencies x channels]
%   - Pk        label vector (Only two classes are allowed) [samples x 1]
%   - NSTD      Outlayers removal. Number of standard deviation to 
%               consider a sample as outlayer 
%   - do_balance balancing classes [default:false]
%
% It returns a vector F of fisher score. The vector is in the format
% [(channelsxfrequencies) x 1]
%
% SEE ALSO: proc_get_outlayers
    
    % Default value for rmsize (outlayer removal)
    if nargin == 2
        NSTD = [];
        do_balance = false;
    end
    
    if nargin == 3
        do_balance = false;
    end

    % Check number of classes
    Classes = unique(Pk);
    NumClasses = length(Classes);
    
    if NumClasses ~= 2
        error('chk:classes', 'Number of classes must be 2');
    end
    
    % Check dimensionality
    if isequal(size(P, 1), length(Pk)) == false
        error('chk:dim', 'First dimension of P and length of Pk must be the same');
    end
    
    % Reshaping data matrix [samples x (channels x frequencies)]
    if ndims(P) == 3
        rP = proc_reshape_ts_bc(P);
    elseif ismatrix(P)
        rP = P;
    else
        error('chk:dim', 'P must have 3 dimensions (samples x freqs x chans) or 2 dimensions (samples x (freqxchans))');
    end
    
    % Class indexes
    Index1 = find(Pk == Classes(1));
    Index2 = find(Pk == Classes(2));
    
    % Balancing classes
    
    if (do_balance == true) && (isequal(length(Index1), length(Index2)) == false)
       [mlength, mclass] = min([length(Index1) length(Index2)]); 
       
       if(mclass == 1)
           Index2 = Index2(randperm(length(Index2), mlength));
           %Index2 = Index2(1:mlength);
       elseif(mclass == 2)
           Index1 = Index1(randperm(length(Index1), mlength));
           %Index1 = Index1(1:mlength);
       end
    end
    
    
    F = zeros(size(rP, 2), 1);
    for fId = 1:size(rP, 2)
        
        % Getting current data for each class and the given feature
        cdata1 = rP(Index1, fId);
        cdata2 = rP(Index2, fId);
        
        % If rmsize is provided, remove outlayers per class
        if isempty(NSTD) == false
            out1 = proc_get_outlayers(cdata1, NSTD);
            out2 = proc_get_outlayers(cdata2, NSTD);
            cdata1 = cdata1(out1 == false);
            cdata2 = cdata2(out2 == false);
        end
        
        % Computing mean and standard deviation for each class
        m1 = nanmean(cdata1);
        s1 = nanstd(cdata1);
        
        m2 = nanmean(cdata2);
        s2 = nanstd(cdata2);
        
        % Computing feature score for the given feature
        F(fId) = abs(m2 - m1) ./ sqrt(s1.^2 + s2.^2);
        %F(fId) = abs(m2 - m1).^2 ./ sqrt(s1.^2 + s2.^2);
    end

end
