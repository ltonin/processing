function [Pxx, f] = proc_psd(s, window, noverlap, nfft, fs)

    if ndims(s) ~=2
        error('chk:input', 'Input signal must be in the format points x channels');
    end

    NumChannels = size(s, 2);
   
    
    if length(nfft) > 1
        Pxx = zeros(length(nfft), NumChannels);
    else
        Pxx = [];
    end
    
    for chIdx = 1:NumChannels
       
        csig = squeeze(s(:, chIdx));
        
        [cpsd, f] = pwelch(csig, window, noverlap, nfft, fs);
        Pxx(:, chIdx) = cpsd;
        
    end
end
