function [WinPos, WinSig] = proc_thresholding(s, th)
% [WinPos, WinSig] = proc_thresholding(s, th)
%
% Given a signal s and a threshold th, this function thesholds the signal
% according to the threshold and it returns the position of the resulting
% window and the logical window signal. It also interpolate the original
% signal over NaN values.

    % Window signal
    WinSig = s > th;
    
    % Window positions
    WinPos(:, 1) = find(diff(WinSig) == 1);
    WinPos(:, 2) = find(diff(WinSig) == -1);

end