function [values, indexes] = proc_features_selection(features, stype, sparam)
% [values, indexes] = proc_features_selection(features, stype, sparam)
% 
%   features:   features vector
%   stype:      'percentile', 'threshold' or 'number'
%   sparam:     parameter for the selection (according to the type)
%
%   values:     values of the selected features
%   indexes:    indexes of the selected features
%   
%   It computes features selection on vector 'features' based on three algorithms:
%       1 - 'percentile'        Selection based on the percentile provided with
%                               respect to the sum of all features
%       2 - 'threshold'         Selection based on the threshold provided with
%                               respect to the values for all features
%       3 - 'greatests'         Selection based on the number of features provided
%                               with respect to the greatest features


    if (nargin ~= 3)
        error('chk:in', 'Argument number incorrect');
    end
    
    if ischar(stype) == false
        error('chk:in', 'Second argument must indicate selection type (stype: string)');
    end
    
    if isnumeric(sparam) == false || numel(sparam) > 1
        error('chk:in', 'Third argument must indicate selection parameter (sparam: number)');
    end
    
    
    switch stype
        case 'percentile'
            [values, indexes] = select_onpercentile(features, sparam);
        case 'threshold'
            [values, indexes] = select_onthreshold(features, sparam);
        case 'greatests'
            [values, indexes] = select_ongreatests(features, sparam);
        otherwise
            error('ch:in', 'Selection type not implemented');
    end
    
end

function [values, indexes] = select_onpercentile(features, percentile) 
    
    [svalues, sindex] = sort(features, 'descend');
    scontribution = 100*cumsum(svalues)./sum(svalues);
    percentileId = find(scontribution <= percentile, 1, 'last'); 
    
    indexes = sindex(1:percentileId);
    values  = svalues(1:percentileId);
end

function [values, indexes] = select_onthreshold(features, threshold) 

    [svalues, sindex] = sort(features, 'descend');
    thresholdId = find(svalues >= threshold);
    
    indexes = sindex(thresholdId);
    values  = svalues(thresholdId);
end

function [values, indexes] = select_ongreatests(features, number) 
    
    [~, sindex] = sort(features, 'descend');
    greatestsId = sindex(1:number);
    
    indexes = greatestsId;
    values  = features(greatestsId);
end