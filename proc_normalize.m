function normv = proc_normalize(v, outrange, inrange)

    % Check outrange argument
    if length(outrange) ~= 2
        error('chk:arg', 'Provide lower and upper limit in outrange argument');
    end
    
    if diff(outrange) <= 0
        error('chk:arg', 'Provide correct lower and upper limit in outrange argument');
    end
    
    
    % Check inrange argument
    if length(v) == 1 && nargin < 3
        error('chk:arg', 'With length(v)=1, inrange is required');
    end
    
    if nargin < 3
        inrange = [min(v) max(v)];
    end
    
    if length(inrange) ~= 2
        error('chk:arg', 'Provide lower and upper limit in inrange argument');
    end
    
    if diff(inrange) <= 0
        error('chk:arg', 'Provide correct lower and upper limit in inrange argument');
    end
    
    if sum(v < inrange(1)) ~= 0 || sum(v > inrange(2)) ~= 0
        error('chk:arg', 'v elements are outside inrange');
    end
    
   
    % Compute normalization
    
    % Normalize to [0, 1]:
    range1 = inrange(2) - inrange(1);
    v1 = (v - inrange(1)) / range1;

    % Then scale to [lower,upper]:
    range2 = outrange(2) - outrange(1);
    normv = (v1*range2) + outrange(1);

end