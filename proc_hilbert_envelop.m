function senv = proc_hilbert_envelop(s, order, band, samplerate)


    nsamples  = size(s, 1);
    nchannels = size(s, 2);
    
    
    % Filters parameters
    [b, a] = butter(order, band*2/samplerate);
    
    % Applying filter and hilbert
    %sfilt = nan(nsamples, nchannels);
    senv = nan(nsamples, nchannels);
    for chId = 1:nchannels
        
        cfilter = filtfilt(b, a, s(:, chId));
        %sfilt(:, chId) = cfilter;
        senv(:, chId) = abs(hilbert(cfilter));
    end

end
