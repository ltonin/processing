function [psd, frequencies] = proc_periodgram(s, selfreqs, fs, dolog)
% [psd, frequencies] = proc_pwelch(s, window, overlap, selfreqs, fs, [dolog])
%
% Computes the pwelch of the input signal for each channel. 
% Optional output is the vector of frequencies.

    if nargin < 6
        dolog = true;
    end

    NumFreqs    = length(selfreqs);
    NumChannels = size(s, 2);
    psd = zeros(NumFreqs, NumChannels);
    
    for chId = 1:NumChannels
       
        [pxx, frqs] = periodogram(s(:, chId), hann(size(s, 1)), [], fs);
       
        [~, set] = intersect(frqs, selfreqs);
        
        if(length(set) ~= length(selfreqs))
            warning('chk:frq', '[proc_periodogram] Cannot provide requested frequencies');
        end
        
        cpsd = pxx(set);
        
        if dolog
            cpsd = log10(cpsd);
        end
        
        psd(:, chId) = cpsd;
        
    end
    
    if nargout > 1
        frequencies = frqs(set);
    end
    

end