function [wcoeff, scales] = proc_cmor_wavelet(x, wltype, fs, freqrange, scalestep, mirrored)

    if nargin < 6
        mirrored = 0;
    end

    fc = centfrq(wltype);
    scalerange = fc./(freqrange*(1/fs));

    scales = scalerange(end):scalestep:scalerange(1);
    

    
    NumChannels = size(x, 2);
    
    if mirrored
        [xD, del] = proc_mirror(x);
    else
        xD = x;
    end
        
        
    wcoeff = [];
    for chId = 1:NumChannels
        ccoeff = cwt(xD(:, chId), scales, wltype);
        
        if mirrored
            ccoeff = ccoeff(:, del(1):del(2));
        end
        
        wcoeff = cat(3, wcoeff, ccoeff);
    end


    
end