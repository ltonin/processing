function [psd, settings] = proc_simonline_pwelch(s, fs, fsize, bsize, wsize, wovl)
% [psd, settings] = proc_simonline_pwelch(s, fs, fsize, bsize, wsize, wovl)
%
% This function simulates the online processing loop with following step
% (in this order):
%   1 - Ring buffer (length: bsize) filled at each iteration with a frame 
%       of the data s (length: fsize)
%   2 - Pwelch computation with hamming window (length: wsize*bsize) and
%       window overlap (wsize*bsize*wovl).
% 
% Spatial filtering (e.g., CAR, Laplacian, CSP) is not applied since 
% usually does not depend on temporal dimension and can be safetely applied
% before.
%
% Input arguments:
%
%   s               Data to be processed [samples x channels]
%   fs              Sample rate of the data
%   fsize           Length of frame size of the data to be added to
%                   ringbuffer [in sample]
%   bsize           Length of the ringbuffer [in sample]
%   wsize           Length of the hamming window to be applied for pwelch.
%                   Value between [0 1] related to the bsize (length of the
%                   ringbuffer)
%   wovl            Overlap of the hamming windows to be applied for
%                   pwelch. Value between [0 1] related to the wsize
%                   (length of the psd hamming window).
%
% Output arguments:
%
%   psd             Three dimensional matrix with psd value computed on the 
%                   ringbufferat each frame 
%                   [samples x frequencies x channels]
%   settings        Settings structure of psd data with following
%                   information:
%                   .fs         Sample Rate [Hz]
%                   .bsize      Length of the ringbuffer [samples]
%                   .fsize      Length of the frame [samples]
%                   .wsize      Length of the PSD hamming window [samples]
%                   .wovl       Overlapping sample for PSD windows [samples]
%                   .nfft       Number of FFT points where PSD has been
%                               computed.
%                   .f          Vector with frequency values where PSD has 
%                               been computed [frequency x 1]
%
% Example:
%
% Simulated online processing for 10 seconds of sinwave (fc = 10Hz) with 
% samplerate of 512 Hz. Ringbuffer size is set to 1 second, frame size at 
% 0.0625 seconds. Pwelch window is set to 0.5 seconds and pwelch window 
% overlap at 0.25 seconds.
%
% fs      = 512;             % Hertz
% T       = 10;              % seconds
% t       = 0:1/fs:T-1/fs;
% fc      = 10;              % Hertz
% s       = repmat(sin(t*fc*2*pi), [16 1])';
% fsize   = 0.0625;   % seconds
% bsize   = 1;        % seconds
% wsize   = 0.5;
% wovl    = 0.5;
%
% [psd, settings] = proc_simonline_pwelch(s, fs, fsize, bsize, wsize, wovl);
%
% SEE ALSO: proc_ringbuffer_init, proc_ringbuffer_add, pwelch

    NumSamples  = size(s, 1);
    NumChannels = size(s, 2);

    FrameSize   = floor(fsize*fs);
    BufferSize  = floor(bsize*fs);
    WinSize     = floor(wsize*bsize*fs);
    WinOverlap  = floor(WinSize*wovl);
    [NFFT, NumFreqs] = proc_getNFFT(WinSize);
    
    FrameStart = 1:FrameSize:(NumSamples - FrameSize);
    FrameStop  = FrameStart + FrameSize - 1;
    NumFrames  = length(FrameStart);
    
    psd     = zeros(NumFrames, NumFreqs, NumChannels);
    rbuffer = proc_ringbuffer_init(BufferSize, NumChannels);
    
    fprintf('[proc] - Pwelch simonline process settings:\n');
    fprintf('         - Sample rate:        %d\n', fs);
    fprintf('         - Frame size:         %d\n', FrameSize);
    fprintf('         - Buffer size:        %d\n', BufferSize);
    fprintf('         - PSD window size:    %d\n', WinSize);
    fprintf('         - PSD window overlap: %d\n', WinOverlap);
    fprintf('         - PSD window type:    hamming\n');
    fprintf('         - PSD NFFT:           %d\n', NFFT);
    fprintf('         - PSD Frequency:      %d\n', NumFreqs);
    
    fprintf('[proc] - Processing each frame:\n');
    for frId = 1:NumFrames
        util_disp_progress(frId, NumFrames, '        ');
        cstart = FrameStart(frId);
        cstop  = FrameStop(frId);
        
        cframe = s(cstart:cstop, :);
        
        [rbuffer, isfull] = proc_ringbuffer_add(rbuffer, cframe);
        
        if isfull == false
            continue;
        end

        for chId = 1:NumChannels 
            [psd(frId, :, chId), f] = pwelch(rbuffer(:, chId), WinSize, WinOverlap, NFFT, fs);     
        end
        
    end
    
    settings.type    = 'pwelch';
    settings.fs      = fs;
    settings.bsize   = BufferSize;
    settings.fsize   = FrameSize;
    settings.wsize   = WinSize;
    settings.wovl    = WinOverlap;
    settings.nfft    = NFFT;
    settings.f       = f;   
end