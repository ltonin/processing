function [s, delimiter] = proc_demirror(mirr_s)
% [s, delimiter] = proc_demirror(mirr_s)
%
% This function demirrors a signal previously mirrored by proc_mirror. It
% returns a cut version of the signal. The output signal is 1/3 times the
% input signal.
%
% SEE ALSO: proc_demirror
    
    LengthMirror = size(mirr_s, 1);
    LengthOriginal = floor(LengthMirror./3);
    
    delimiter = [LengthOriginal+1 2*LengthOriginal];
    s = mirr_s(LengthOriginal + 1:2*LengthOriginal, :);
    
    
end