function [Dp, IdSelected, DpRun] = proc_cva_selection(F, Fk, Rk, NumSelected, optdraw, srcsize)
% [Dp, IdSelected, DpRun] = proc_cva_selection(F, Fk [, Rk, NumSelected, optdraw, srcsize])
%
% Function to compute discriminant power and stability of the features
% across different runs.
%
% F: is in the format [observation x features].
%
% Fk: is labels for each observation and it is in the format [observation x 1].
%
% Rk: is a label vector with the index of the run [observation x 1]. If
% empty, the function consider all the observation coming from the same
% run.
%
% NumSelected: number of selected features. If empty, the function
% considers all the features selected.
%
% optdraw: if equal to 'draw' the function plot the dp of each run and the
% overall one.
%
% scrsize: required argument if optdraw='draw' is provided. Size of the
% original data (e.g., Number of Frequency and Number of Channels).

    if(isequal(size(F, 1), length(Fk)) == false)
        error('chk:arg', 'F and Fk must have the same number of rows');
    end

    if isempty(Rk) == true
        Rk = ones(length(Fk), 1);
    end
    
    if(isequal(length(Rk), length(Fk)) == false)
        error('chk:arg', 'Fk and Rk must have the same number of rows');
    end
    
    if nargin < 4
        NumSelected = size(F, 2);
        optdraw = [];
        srcsize = [];
    end
    
    if nargin == 5
        error('chk:arg', 'Option draw requires additional argument with dimension of original data');
    end
    
    Runs        = unique(Rk);
    NumRuns     = length(Runs);
    NumFeatures = size(F, 2);
    
    DpRun = zeros(NumFeatures, NumRuns);
    DpNom = zeros(NumFeatures, 1);
    DpDen = 0;
    for rId = 1:NumRuns
        cindex  = Rk == Runs(rId);
        [cdp, cstrmat, cgamma] = proc_cva(F(cindex, :), Fk(cindex));
        cvec    = ((cstrmat).^2)*cgamma;
        DpNom   = DpNom + cvec;
        DpDen   = DpDen + sum(cvec);
        DpRun(:, rId) = cdp;
    end

    Dp = 100.*(DpNom/DpDen);
    
    [~, idsort] = sort(Dp, 'descend');
    IdSelected = sort(idsort(1:NumSelected));
    
    if(strcmp(optdraw, 'draw'))
        figure;
        for rId = 1:NumRuns
            subplot(2, NumRuns, rId); 
            imagesc(reshape(DpRun(:, rId), [srcsize(1) srcsize(2)])'); 
            ylabel('Channels');
            xlabel('Frequency Id');
            set(gca, 'XTick', 1:srcsize(1));
            set(gca, 'YTick', 1:srcsize(2));
            title(['Run ' num2str(rId)]);
        end; 
        
        
        subplot(2, NumRuns, (NumRuns + 1):(NumRuns + ceil(NumRuns/2)));
        imagesc(reshape(Dp, [srcsize(1) srcsize(2)])'); 
        set(gca, 'XTick', 1:srcsize(1));
        set(gca, 'YTick', 1:srcsize(2));
        xlabel('Frequency Id');
        ylabel('Channels');
        colorbar;
        
                hold on;
        if(isequal(length(IdSelected), NumFeatures) == false)
            for i = 1:length(IdSelected)
                [cx, cy] = ind2sub([srcsize(1) srcsize(2)], IdSelected(i));
                plot(cx, cy, 'ok', 'LineWidth', 3);
            end
        end
        
        hold off;
        title(['Overall dp - ' num2str(length(IdSelected)) ' features selected']);
        
    end

end