function [dp, strmat, gamma] = proc_cva(F, Fk)
% [dp, strmat, gamma] = proc_cva(F, Fk)
%
% Wrapper for cva_tun_opt. F is in the format [observation x features].
% Fk is labels for each observation and it is in the format [observation x 1].
%
% Output:
%
%   dp      vector with discriminant power for each feature
%   strmat  Structure from cva_tun_opt
%   gamma   Normalized eigenvalue from cva_tun_opt


    if(isequal(length(unique(Fk)), 2) == false)
        error('chk:cls', ['Only two labels are allowed. ' num2str(length(unique(Fk))) ' in Fk']);
    end
    
    [dp, strmat, ~, gamma] = cva_tun_opt(F, Fk);
    
end

