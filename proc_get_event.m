function [label, pos, dur] = proc_get_event(evtstr, event, nsamples, fixdur, offset)
% [label, pos, dur] = proc_get_event(evtstr, event, nsamples [, fixdur, offset])
%
% The function create a logical vector related to the provided event type.
%
% evtstr            Structure in GDF format with the following fields:
%                   - .TYP  -> Type of events
%                   - .POS  -> Positions of events
%                   - .DUR  -> Duration of event (optional if fixed
%                              duration argument is provided)
%
% nsamples          Total length in samples of the label vector
% 
% event             Value of the requested event
%
% fixdur            Set fixed duration of each event [OPTIONAL: default evtstr.DUR]
%                   The duration is always referred to the position of the
%                   event
%
% offset            Add an offset to the start event position [OPTIONAL: 
%                   default 0]. The offset is always referred to the 
%                   original starting position of the event. This means
%                   that if both fixdur and offset are provided, the label
%                   will have a logical high value for fixdur-offset
%                   samples.
%
% Output:
%
% label             logical label vector related to the provided event type
%
% pos               Positions of the onsets related to the provided event type
%
% dur               Duration of the onsets related to the provided event type

    if nargin == 3
        fixdur = [];
        offset = 0;
    end
    
    if nargin == 4
        offset = 0;
    end

    % Check if event struct has TYP field
    if isfield(evtstr, 'TYP') == false
        error('chk:evt', ['[' mfilename '] Struct events does not have TYP field']);
    end
    
    % Check if event struct has POS field
    if isfield(evtstr, 'POS') == false
        error('chk:evt', ['[' mfilename '] Struct events does not have POS field']);
    end
    
    % Check if event struct has DUR field or it duration is provided as argument
    if (isfield(evtstr, 'DUR') == false) && (isempty(fixdur) == true)
        error('chk:evt', ['[' mfilename '] Struct events does not have DUR field, please provide duration as argument']);
    end
    
    % Check if the requested event type is in the event struct
    if isempty(find(evtstr.TYP == event, 1)) == true
        warning('chk:evt', ['[' mfilename '] No event type equal to ' num2str(event) ' found in event structure']);
    end
    
    % Extract required event position and duration from the structure
    evtpos = evtstr.POS(evtstr.TYP == event);
    nevt   = length(evtpos);
    if isempty(fixdur) == false
        evtdur = fixdur*ones(nevt, 1);
    else
        evtdur = evtstr.DUR(evtstr.TYP == event);
    end
    
    evtdur(evtdur == 0) = 1;
    
    % Create label vector for the provided event typ
    label  = false(nsamples, 1); 
    pos    = zeros(nevt, 1);
    dur    = zeros(nevt, 1);
    for eId = 1:nevt
        if offset >= evtdur(eId)
            warning('chk:offset', ['[' mfilename '] Offset (' num2str(offset) ') for ' num2str(eId) 'th event is bigger than the event duration (' num2str(evtdur(eId)) ')']);
        end
        cstart = evtpos(eId) + offset;
        cstop  = evtpos(eId) + evtdur(eId) - 1;
        label(cstart:cstop) = true;
        pos(eId) = cstart;
        dur(eId) = length(cstart:cstop);
    end

end