function [label, pos, dur] = proc_getevents(evtstr, nsamples, evttyp, fixdur, req_offset)
% [label, pos, dur] = proc_getevents(evtstr, nsamples, evttyp, [fixdur])
%
% The function create a logical vector related to the provided event type.
%
% evtstr            Structure in GDF format with the following fields:
%                   - .TYP  -> Type of events
%                   - .POS  -> Positions of events
%                   - .DUR  -> Duration of event (optional if fixed
%                              duration argument is provided)
%
% nsamples          Total length in samples of the label vector
% 
% evttyp            Type of the requested event
%
% fixdur            Set fixed duration of each event [OPTIONAL: default evtstr.DUR]
%
% req_offset        Set custom start position [OPTIONAL: default 0]
%
% Output:
%
% label             logical label vector related to the provided event type
%
% pos               Positions of the onsets related to the provided event type
%
% dur               Duration of the onsets related to the provided event type

warning('OBSOLETE! Use proc_event_get instead!');

    % Check if event struct has TYP field and thus, extract event equals to
    % the request type
    if isfield(evtstr, 'TYP') == false
        error('chk:evt', 'Struct events does not have TYP field');
    else
        idxtyp = evtstr.TYP == evttyp;
        if sum(idxtyp) == 0
            warning('chk:evt', ['No event type equal to ' num2str(evttyp) ' found in Struct events']);
            label = NaN;
            return
        end
    end

    % Check if event struct has POS field and thus, extract the position
    % related to the requested type
    if isfield(evtstr, 'POS') == false%     if (nargin == 4) || (nargin == 5)
%         evtdur(:) = fixdur;
%     end
%     
%     if nargin < 5
%         offset = 0;
%     end
%     

        error('chk:evt', 'Struct events does not have POS field');
    else
        evtpos = evtstr.POS(idxtyp);
    end

    numevt = length(evtpos);
    
    % Check if event struct has DUR field and thus, extract the duration
    % related to the requested type. Check if fixed duration is provided
    % and thus, use it as default event duration
    if (isfield(evtstr, 'DUR') == false) 
        error('chk:evt', 'Struct events does not have DUR field and fixed Duration has not be provided');
    end
        
    
    if (nargin == 4)
        offset = 0;
        %evtdur(1:numevt) = fixdur;
    elseif (nargin == 5)
        offset = req_offset +1;
        %evtdur(1:numevt) = fixdur;
    else
        offset = 0;
        fixdur = [];
        %evtdur = evtstr.DUR(idxtyp);
    end
    
    if isempty(fixdur)
        evtdur = evtstr.DUR(idxtyp);
    else
        evtdur(1:numevt) = fixdur;
    end
   
    % Create label vector for the provided event typ
    label  = false(nsamples, 1); 
    pos    = zeros(numevt, 1);
    dur    = zeros(numevt, 1);
    for eId = 1:numevt
        cstart = evtpos(eId) + offset;
        cstop  = evtpos(eId) + evtdur(eId) - 1;
        %cstop  = cstart + evtdur(eId) - 1;
        label(cstart:cstop) = true;
        pos(eId) = cstart;
        %dur(eId) = evtdur(eId);
        dur(eId) = length(cstart:cstop);
    end

end