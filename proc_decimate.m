function [down_s] = proc_decimate(s, infreq, outfreq)
% [down_s] = proc_decimate(s, infreq, outfreq)
%
% This function downsamples the ANALOG input data (i.e. EEG channels). 
% infreq and outfreq correspond to the source and the output frequency 
% respectevely. Input data must be in the format [points x channels]
%
%
% SEE ALSO: proc_downsample, decimate, downsample, io_downsample_bdf

    DownRatio = round(infreq/outfreq);

    NumSamples = size(s, 1);
    NumChannels = size(s, 2);
    down_s = zeros(round(NumSamples/DownRatio), NumChannels);
    
    for chId = 1:NumChannels
       down_s(:, chId) = decimate(s(:, chId), DownRatio);
    end
        

end