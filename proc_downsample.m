function [down_d] = proc_downsample(d, infreq, outfreq)
% [down_d] = proc_downsample(d, infreq, outfreq)
%
% This function downsamples the ANALOG input data (i.e. trigger channels). 
% infreq and outfreq correspond to the source and the output frequency 
% respectevely. Input data must be in the format [points x channels]
%
%
% SEE ALSO: proc_downsample, decimate, downsample, io_downsample_bdf
    DownRatio = round(infreq/outfreq);
    
    NumSamples = size(d, 1);
    NumChannels = size(d, 2);
    down_d = zeros(round(NumSamples/DownRatio), NumChannels);

    for chId = 1:NumChannels
       down_d(:, chId) = downsample(d, DownRatio);
    end


end