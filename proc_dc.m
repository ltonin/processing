function [dataout, dc] = proc_dc(datain)
% out = proc_dc(in)
%
% Removes dc value from the data. 
% datain is a [points x channels] matrix

    dc = mean(datain, 1);
    dataout = datain - repmat(dc, size(datain, 1), 1);
    
end