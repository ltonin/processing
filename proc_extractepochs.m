function [epochs] = proc_extractepochs(signal, trigger, period, fs)
% [epochs] = proc_extractepochs(signal, trigger, period, fs)
%
% This function extract portion of signals (epochs) from a given input
% signal, according to the trigger positions.
%
% signal        -- matrix points x channels
% trigger       -- vector with trigger position
% period        -- Start and stop time of the epoch (negative value are
%                  considered before the trigger)
% fs            -- sampling frequency of the signal
%
% This function returns a matrix points x channels x trials

    %warning('chk:debug', 'NOT TESTED YET!!!!!');
    
    if ((period(1) > period(2)))
        error('chk:period', 'Period values not consistent');
    end
    
   
    
    NumSamples  = diff(period*fs) + 1;
    NumChannels = size(signal, 2);
    NumTrials  = length(trigger);
    
    
    epochs = zeros(NumSamples, NumChannels, NumTrials);

    
    
    for TrId = 1:NumTrials
        
        c_trig = trigger(TrId);
        
        StartSample = round(c_trig + period(1)*fs) + 1;
        StopSample  = round(c_trig + period(2)*fs) + 1;

        epochs(:, :, TrId) = signal(StartSample:StopSample, :);
     
    end
    


end