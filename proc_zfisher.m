function z = proc_zfisher(r)
% z = proc_zfisher(r)
%
% The function provides the fisher z-transformed correlation coefficient r
% r must be a vector.


    if(isvector(r) == false)
        error('chk:typ', 'r must be a vector');
    end
    
    z = 0.5.*log((1+r)./(1-r));

end