function sOutput = proc_resample(s,postFs, preFs, mirrored)

    if nargin < 4
        mirrored = 0;
    end

    NumChannels = size(s, 2);
    
    if mirrored 
        sD = proc_mirror(s);
    else
        sD = s;
    end
    
    sR = [];
    for chId = 1:NumChannels
    
        cs = sD(:, chId);
        
        sR = cat(2, sR, resample(cs, postFs, preFs));
    
    
    end
    
    if mirrored 
        sOutput = proc_demirror(sR);
    else
        sOutput = sR;
    end
        
        

end