function id = proc_get_channel(key, list)
% id = proc_get_channel(key, list)
%
% The function returns the identifier of the channel KEY according to the 
% LIST provided as a argument. Request of multiple keys is allowed. KEY can
% be a vector with the requested channel indexes, a string with the
% requested channel name or a cell array with the requested channel names.
% According to the type of argument KEY, the output identifier will be a
% cell array with names or a vector with channel indexes, respectively.
%
% SEE ALSO: util_cellfind

    if ischar(key)
        key = {key};
    end
    
    if isnumeric(key)
        id = getbyid(key, list);
    elseif iscell(key)
        id = getbylabel(key, list);
    end

end

% Get channels by key-labels
function id = getbylabel(key, list)
    id = util_cellfind(upper(key), upper(list));
end

% Get channels by key-index
function id = getbyid(key, list)
    id = list(key);
end