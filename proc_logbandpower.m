function slog = proc_logbandpower(s, order, band, winsize, samplerate)
% PROC_LOGBANDPOWER(s, order, band, winsize, samplerate) computes the
% logarithmic band power of the signal by applying a bandpass filter,
% rectification, moving average and log transform
%
%   s           [samples x channels]
%   order       Filter order (butterworth)
%   band        Band for the bandpass filter
%   winsize     Size in second of the moving average window
%   samplerate  Samplerate of the signal s

    nsamples  = size(s, 1);
    nchannels = size(s, 2);
    
    sfilt = nan(nsamples, nchannels);
    
    % Filters parameters
    [b, a] = butter(order, band*2/samplerate);
    
    % Applying filter
    for chId = 1:nchannels
        sfilt(:, chId) = filtfilt(b, a, s(:, chId));
    end
    
    % Squaring
    srect = power(sfilt, 2);
    
    % Moving average
    smov = nan(nsamples, nchannels);
    for chId = 1:nchannels
        smov(:, chId) = (filter(ones(1, winsize*samplerate)/winsize/samplerate, 1, srect(:, chId)));
    end
    keyboard
    % Logarithmic transformation
    slog = log(smov);
    
end
