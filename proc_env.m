function [s_envel, s_phase, s_hilb] = proc_env(s)
% [s_envel, s_phase, s_hilb] = proc_env(s)
%
% Computes the envelop of the input signal by using the absolute value of
% the hilbert transformation. It returns also the phase and the hilbert
% transformation itself.
% Input signal must be in the format points x channels


    if ndims(s) ~=2
        error('chk:input', 'Input signal must be in the format points x channels');
    end

    NumSamples  = size(s, 1);
    NumChannels = size(s, 2);
    
    s_envel = zeros(NumSamples, NumChannels);
    s_phase = zeros(NumSamples, NumChannels);
    s_hilb  = zeros(NumSamples, NumChannels);
    
    for chIdx = 1:NumChannels
        
        c_signal = squeeze(s(:, chIdx));

        
        c_hilb   = hilbert(c_signal);
        c_envel  = abs(c_hilb);
        c_phase  = angle(c_hilb);

        s_envel(:, chIdx) = c_envel;
        s_phase(:, chIdx) = c_phase;
        s_hilb(:, chIdx) = c_hilb;
                  
        
    end

end