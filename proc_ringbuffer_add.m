function [obuffer, isfull] = proc_ringbuffer_add(buffer, frame)

    if isequal(size(buffer, 2), size(frame, 2)) == false
        error('chk:dim', 'frame must have the same number of column of buffer');
    end
    
    if size(frame, 1) > size(buffer, 1)
        error('chk:dim', 'frame must not have more rows than buffer');
    end
    
    NumRows   = size(buffer, 1);
    NumCols   = size(buffer, 2);
    FrameSize = size(frame, 1);
    
    
    obuffer = nan(NumRows, NumCols);
    obuffer(1:end-FrameSize, :) = buffer(FrameSize+1:end, :);
    obuffer(end-FrameSize + 1:end, :) = frame;
    
    isfull = false;
    if sum(sum(isnan(obuffer))) == 0
        isfull = true;
    end
    

end