function [Freqs, Chans] = proc_bin2bc(size, indexes)

    NumBins = length(indexes);
    
    Freqs = zeros(NumBins, 1);
    Chans = zeros(NumBins, 1);
    
    for b = 1:NumBins
        
        [Freqs(b), Chans(b)] = ind2sub(size, indexes(b));
    end
        
        

end